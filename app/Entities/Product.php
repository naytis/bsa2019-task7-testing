<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Entities
 * @property int $id
 * @property string $name
 * @property float $price
 * @property int $user_id
 */
final class Product extends Model
{
    protected $fillable = [
        'id',
        'name',
        'price',
        'user_id'
    ];
}
