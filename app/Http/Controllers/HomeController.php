<?php

namespace App\Http\Controllers;

use App\Services\MarketService;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->middleware('auth');
        $this->marketService = $marketService;
    }

    public function index()
    {
        $user = Auth::user();
        $products = $this->marketService->getProductsByUserId($user->id);

        return view('home', compact('products'));
    }
}
