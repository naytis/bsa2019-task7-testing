<?php

namespace App\Http\Controllers;

use App\Http\Actions\StoreProductRequest;
use App\Http\Requests\StoreProductHttpRequest;
use App\Http\Resources\ProductResource;
use App\Http\Response\ApiResponse;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketApiController extends ApiController
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList(): ApiResponse
    {
        $products = $this->marketService->getProductList();
        return $this->createSuccessResponse(ProductResource::collection($products));
    }

    public function store(StoreProductHttpRequest $request): ApiResponse
    {
        $product = $this->marketService->storeProduct(
            new StoreProductRequest(
                $request->input('name'),
                $request->input('price'),
                $request->input('user_id')
            )
        );

        return $this->created(new ProductResource($product));
    }

    public function showProduct(int $id): ApiResponse
    {
        $product = $this->marketService->getProductById($id);
        return $this->createSuccessResponse(new ProductResource($product));
    }

    public function delete(Request $request): ApiResponse
    {
        $this->marketService->deleteProduct($request);
        return $this->createDeletedResponse();
    }
}
