<?php

namespace App\Http\Controllers;

use App\Http\Actions\StoreProductRequest;
use App\Http\Requests\StoreProductHttpRequest;
use Illuminate\Http\Request;
use App\Services\MarketService;
use Illuminate\Support\Facades\Auth;

class MarketWebController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showMarket()
    {
        $products = $this->marketService->getProductList();

        return view('market', compact('products'));
    }

    public function showProduct(int $id)
    {
        try {
            $product = $this->marketService->getProductById($id);
        } catch (\Exception $e) {
            return redirect()->route('main');
        }

        return view('product', compact('product'));
    }

    public function addProductForm()
    {
        return view('addProductForm');
    }

    public function storeProduct(StoreProductHttpRequest $request)
    {
        $this->marketService->storeProduct(
            new StoreProductRequest(
                $request->input('product_name'),
                $request->input('product_price'),
                Auth::id()
            )
        );

        return redirect()->route('main');
    }

    public function deleteProduct(Request $request)
    {
        $this->marketService->deleteProduct($request);

        return redirect()->route('main');
    }
}