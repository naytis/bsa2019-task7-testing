<?php

namespace App\Http\Actions;

class StoreProductRequest
{
    private $name;
    private $price;
    private $userId;

    public function __construct(string $name, string $price, int $userId)
    {
        $this->name = $name;
        $this->price = $price;
        $this->userId = $userId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): string
    {
        return $this->price;
    }

    public function userId(): int
    {
        return $this->userId;
    }
}