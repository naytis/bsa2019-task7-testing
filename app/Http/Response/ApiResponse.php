<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

final class ApiResponse extends JsonResponse
{
    public const CLIENT_ERROR_STATUS = 400;
    public const NO_CONTENT_STATUS = 204;
    public const FORBIDDEN_STATUS = 403;
    public const RESOURCE_CREATED_STATUS = 201;

    public static function error(int $code, string $message): self
    {
        static::assertErrorDataIsValid($code, $message);

        return new static([
            'errors' => [
                [
                    'code' => $code,
                    'message' => $message
                ]
            ]
        ], self::CLIENT_ERROR_STATUS);
    }

    public static function success(JsonResource $data): self
    {
        return new static(['data' => $data]);
    }

    public static function created(JsonResource $data): self
    {
        return new static(['data' => $data], self::RESOURCE_CREATED_STATUS);
    }

    public static function deleted(): self
    {
        return new static(null, self::NO_CONTENT_STATUS);
    }

    public static function notFound(string $message): self
    {
        return static::error(self::CLIENT_ERROR_STATUS, $message);
    }

    private static function assertErrorDataIsValid(string $code, string $message): void
    {
        if (empty($code) || empty($message)) {
            throw new \InvalidArgumentException('Error values cannot be empty.');
        }
    }
}