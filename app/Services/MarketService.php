<?php

namespace App\Services;

use App\Exceptions\ProductNotFound;
use App\Http\Actions\StoreProductRequest;
use App\Entities\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\ProductRepositoryInterface as ProductRepository;

class MarketService
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProductList(): Collection
    {
        return $this->productRepository->findAll();
    }

    public function getProductById(int $id): ?Product
    {
        try {
            $product = $this->productRepository->findById($id);
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFound("No product with id: {$id}");
        }

        return $product;
    }

    public function getProductsByUserId(int $userId): Collection
    {
        return $this->productRepository->findByUserId($userId);
    }

    public function storeProduct(StoreProductRequest $request): Product
    {
        $product = new Product([
            'user_id' => $request->userId(),
            'name'    => $request->name(),
            'price'   => $request->price()
        ]);

        return $this->productRepository->store($product);
    }

    public function deleteProduct(Request $request): void
    {
        try {
            $product = $this->getProductById($request->id);
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFound("No product with id: {$request->id}");
        }

        $this->productRepository->delete($product);
    }
}
