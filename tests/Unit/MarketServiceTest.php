<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\Exceptions\ProductNotFound;
use App\Http\Actions\StoreProductRequest;
use App\Repositories\Interfaces\ProductRepositoryInterface as ProductRepository;
use App\Services\MarketService;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarketServiceTest extends TestCase
{
    private $repositoryStub;

    /**
     * @var MarketService
     */
    private $marketService;

    private $products;

    public function setUp(): void
    {
        parent::setUp();
        $this->repositoryStub = $this->mock(ProductRepository::class);
        $this->marketService = new MarketService($this->repositoryStub);
        $this->products = factory(Product::class, 3)->make(['user_id' => 1]);
    }

    public function testGetProductList()
    {
        $this->repositoryStub
            ->shouldReceive('findAll')
            ->andReturn($this->products);

        $products = $this->marketService->getProductList();

        $this->assertCount(3, $products);
        for ($i = 0; $i < 3; $i++) {
            $this->assertProductEquals($this->products[$i], $products[$i]);
        }
    }

    public function testGetProductById()
    {
        $id = 1;
        $this->repositoryStub
            ->shouldReceive('findById')
            ->with($id)
            ->andReturn($this->products[0]);

        $product = $this->marketService->getProductById($id);

        $this->assertProductEquals($this->products[0], $product);
    }

    public function testGetProductByIdNotFound()
    {
        $id = 1;
        $this->repositoryStub
            ->shouldReceive('findById')
            ->with($id)
            ->andThrow(ProductNotFound::class, "No product with id: $id");

        $this->expectException(ProductNotFound::class);
        $this->expectExceptionMessage("No product with id: $id");

        $this->marketService->getProductById($id);
    }

    public function testGetProductsByUserId()
    {
        $userId = 1;
        $this->repositoryStub
            ->shouldReceive('findByUserId')
            ->with($userId)
            ->andReturn($this->products);

        $products = $this->marketService->getProductsByUserId($userId);
        $this->assertCount(3, $products);
        for ($i = 0; $i < 3; $i++) {
            $this->assertProductEquals($this->products[$i], $products[$i]);
        }
    }

    public function testStoreProduct()
    {
        $request = new StoreProductRequest(
            $this->products[1]->name,
            $this->products[1]->price,
            1
        );

        $this->repositoryStub
            ->shouldReceive('store')
            ->andReturn($this->products[1]);

        $product = $this->marketService->storeProduct($request);

        $this->assertProductEquals($this->products[1], $product);
    }

    public function assertProductEquals($productExpected, $productActual)
    {
        $this->assertInstanceOf(Product::class, $productActual);
        $this->assertEquals($productExpected->name, $productActual->name);
        $this->assertEquals($productExpected->price, $productActual->price);
        $this->assertEquals($productExpected->user_id, $productActual->user_id);
    }
}
