<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\Entities\User;
use App\Http\Response\ApiResponse;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;

    public const URL_PREFIX = 'api/items';

    public const ROOT_RESPONSE_KEY = 'data';

    public const PRODUCT_RESOURCE_STRUCTURE = [
        'id',
        'name',
        'price',
        'user_id'
    ];

    public const INVALID_ID = 999;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    public function testShowList()
    {
        $response = $this->get(self::URL_PREFIX)->assertOk();

        $this->assertNotEmpty($response->json('data'));

        $response->assertJsonStructure([
            self::ROOT_RESPONSE_KEY => ['*' => self::PRODUCT_RESOURCE_STRUCTURE]
        ]);
    }

    public function testShowProduct()
    {
        $product = Product::first();

        $uri = $this->createResourceItemUri(self::URL_PREFIX, $product->id);

        $this->get($uri)
            ->assertOk()
            ->assertJsonStructure([
                self::ROOT_RESPONSE_KEY => self::PRODUCT_RESOURCE_STRUCTURE
            ]);
    }

    public function testShowProductNotFound()
    {
        $uri = $this->createResourceItemUri(self::URL_PREFIX, self::INVALID_ID);

        $this->get($uri)
            ->assertStatus(ApiResponse::CLIENT_ERROR_STATUS)
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ApiResponse::CLIENT_ERROR_STATUS,
                        'message' => 'No product with id: ' . self::INVALID_ID
                    ]
                ]
            ]);
    }

    public function testStore()
    {
        $attributes = [
            "name" => "product name",
            "price" => "199.00",
            "user_id" => User::first()->id
        ];

        $this->json('POST', self::URL_PREFIX, $attributes)
            ->assertStatus(201)
            ->assertJsonStructure([
                self::ROOT_RESPONSE_KEY => self::PRODUCT_RESOURCE_STRUCTURE
            ]);
    }

    public function testStoreInvalidParams()
    {
        $attributes = [
            "name" => "product name"
        ];

        $this->json('POST', self::URL_PREFIX, $attributes)
            ->assertStatus(ApiResponse::CLIENT_ERROR_STATUS)
            ->assertJsonStructure(['errors' => []]);
    }

    public function testDelete()
    {
        $order = Product::first();
        $uri = $this->createResourceItemUri(self::URL_PREFIX, $order->id);
        $this->delete($uri)->assertStatus(204);
    }

    public function testDeleteNotFound()
    {
        $uri = $this->createResourceItemUri(self::URL_PREFIX, self::INVALID_ID);

        $this->delete($uri)
            ->assertStatus(ApiResponse::CLIENT_ERROR_STATUS)
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ApiResponse::CLIENT_ERROR_STATUS,
                        'message' => 'No product with id: ' . self::INVALID_ID
                    ]
                ]
            ]);
    }

    private function createResourceItemUri(string $uri, int $id): string
    {
        return $uri . '/' . $id;
    }
}
